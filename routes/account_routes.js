var express = require('express');
var router = express.Router();
var account_dal = require('../model/account_dal');
var insert = require('../model/universalInsert');   //access with url: localhost3000:/account/insert

router.get('/all', function(req, res){
    account_dal.getAll(function(err, response) {
        console.log(response);
        res.render('account/accountViewAll', {response : response});
    });
});


router.post('/all', function(req, res){ //never gets loaded unless is invoked with a post request
    console.log("post req called");
    console.log(req.body.account);//data passed in
    insert.universalInsert("account", req.body.account, function(err, result){
        if(err){
            res.send(err);
        }else{
            res.redirect('/account/all');
        }
    });
});

router.get('/delete/:id', function(req, res){
    console.log(req.params.id);//req.params.id == :id(value passed in url)
    account_dal.delete(req.params.id, function(err, result){
        if(err){
            res.send(err);
        }else{
            res.redirect('/account/all');
        }
    });
});

router.post('/edit', function(req, res){
    console.log("post req called");
    console.log(req.body.account);//data passed in console.log("post req call")
    account_dal.edit(req.body.account, function(err, result){
        if(err){
            console.log("edit threw error");
            res.send(err);
        }else{
            res.redirect('/account/all');
        }
    });
});




module.exports = router;