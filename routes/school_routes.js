var express = require('express');
var router = express.Router();
var insert = require('../model/universalInsert');   //access with url: localhost3000:/school/insert
var school_dal = require('../model/school_dal');


router.get('/all', function(req, res){
    school_dal.getAll(function(err, response){
        //response.map(item => console.log(item));//sanity check
        if(err){
            res.send(err);
        }else{
            res.render('school/schoolViewAll', {response: response});
        }
    });
});

router.post('/all', function(req, res){
    console.log("school: post req invoved, body:");
    console.log(req.body.address);
    insert.universalInsert("school", req.body.school, function(err, result){
        if(err){
            console.log("school: post req threw error");
            res.send(err);
        }else{
            res.redirect('/school/all');
        }
    });
});


//delete call:
router.get('/delete/:id', function(req, res){
    console.log(req.params.id);//req.params.id == :id(value passed in url)
    school_dal.delete(req.params.id, function(err, result){
        if(err){
            res.send(err);
        }else{
            res.redirect('/school/all');
        }
    });
});


//edit call:
router.post('/edit', function(req, res){
    console.log("post req called");
    console.log(req.body.school);//data passed in console.log("post req call")
    school_dal.edit(req.body.school, function(err, result){
        if(err){
            console.log("edit threw error");
            console.log(err);
            res.send(err);
        }else{
            res.redirect('/school/all');
        }
    });
});









module.exports = router;