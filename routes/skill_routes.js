var express = require('express');
var router = express.Router();
var insert = require('../model/universalInsert');   //access with url: localhost3000:/skill/insert
var skill_dal = require('../model/skill_dal');

router.get('/all', function(req, res){
    skill_dal.getAll(function(err, response) {
        //console.log(response);
        res.render('skill/skillViewAll', {response : response});
    });
});

router.post('/all', function(req, res){
    insert.universalInsert("skill", req.body.skill, function(err, result){
        if(err){
            console.log("Adding record error");
            res.send(err);
        }else{
            res.redirect('/skill/all');
        }
    });
});

router.get('/delete/:id', function(req, res){
    console.log(req.params.id);//req.params.id == :id(value passed in url)
    skill_dal.delete(req.params.id, function(err, result){
        if(err){
            res.send(err);
        }else{
            res.redirect('/skill/all');
        }
    });
});

router.post('/edit', function(req, res){
    console.log("post req called");
    console.log(req.body.skill);//data passed in console.log("post req call")
    skill_dal.edit(req.body.skill, function(err, result){
        if(err){
            console.log("edit threw error");
            res.send(err);
        }else{
            res.redirect('/skill/all');
        }
    });
});

module.exports = router;