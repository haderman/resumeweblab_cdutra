var express = require('express');
var router = express.Router();
var address_dal = require('../model/address_dal');
var insert = require('../model/universalInsert');   //access with url: localhost3000:/address/insert


router.get('/all', function(req, res){
    address_dal.getAll(function(err, response) {
        console.log(response);
        res.render('address/addressViewAll', {response : response});
    });
});


router.post('/all', function(req, res){ //never gets loaded unless is invoked with a post request
    console.log("post req called");
    console.log(req.body.address);//data passed in
    insert.universalInsert("address", req.body.address, function(err, result){
        if(err){
            res.send(err);
        }else{
            res.redirect('/address/all');
        }
    });
});

router.get('/delete/:id', function(req, res){
    console.log(req.params.id);//req.params.id == :id(value passed in url)
    address_dal.delete(req.params.id, function(err, result){
        if(err){
            res.send(err);
        }else{
            res.redirect('/address/all');
        }
    });
});

router.post('/edit', function(req, res){
    console.log("post req called");
    console.log(req.body.address);//data passed in console.log("post req call")
    address_dal.edit(req.body.address, function(err, result){
        if(err){
            console.log("edit threw error");
            res.send(err);
        }else{
            res.redirect('/address/all');
        }
    });
});

module.exports = router;