var express = require('express');
var router = express.Router();
var resume_dal = require('../model/resume_dal');
var account_dal = require('../model/account_dal');
var insert = require('../model/universalInsert');   //access with url: localhost3000:/resume/insert

router.get('/add/selectUser', function(req, res){
    account_dal.getAll(function(err, response){
        //console.log("response" + response);
        res.render('resume/resumeSelectUser', {response: response});
    });
});

////res.render('resume/resumeCreateForm', {response1, response2, response3});


router.get('/add/selectUser/:id', function(req, res){
    var id = req.params.id;
    resume_dal.accountSchool(id, function(err1, school){
        resume_dal.accountCompany(id, function(err2, company){
           resume_dal.accountSkill(id, function(err3, skill){
               console.log("response1: " + school);
               console.log("response2: " + company);
               console.log("response3: " + skill);
               var obj = {school:school,    //wrapper obj
                          company: company,
                          skill: skill,
                          id: id};
               console.log('object, school: ' + obj.school);
               res.render('resume/resumeCreateForm', {response: obj});
           })
        });
    })
});

router.get('/insert', function(req, res){//gets called to add a new record.

    console.log('create resume record called');
    console.log(req.query.account_id);
    var obj =   {resume_name: req.query.resume_name,
                account_id: req.query.account_id,
                company_name: req.query.company_name,
                school_name: req.query.school_name,
                skill_name: req.query.skill_name};
    console.log("resume_relevant_obj" + obj);
    resume_dal.insert(obj, function(err, result){
        if(err){
            console.log("resume insert broke");
            res.send(err);
        }else{
            res.send('submittion successful');
        }
    });
});


module.exports = router;