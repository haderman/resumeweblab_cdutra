var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.universalInsert = function(tableName, params, callback) {//param => passed object
    var query = "insert into " + tableName + " set ?";
    connection.query(query, params, function(err, res) {
        callback(err, res);
    });

};